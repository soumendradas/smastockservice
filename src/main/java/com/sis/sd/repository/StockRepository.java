package com.sis.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sis.sd.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long>{

}
