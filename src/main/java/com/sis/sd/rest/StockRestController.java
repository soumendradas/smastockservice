package com.sis.sd.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sis.sd.entity.Stock;
import com.sis.sd.exception.StockNotFoundException;
import com.sis.sd.service.StockService;

@RestController
@RequestMapping("/stock")
public class StockRestController {
	
	@Autowired
	private StockService service;
	
	@PostMapping("save")
	public ResponseEntity<String> saveStock(@RequestBody Stock stock){
		long id = service.addStock(stock);
		return ResponseEntity.ok("Stock created with Id: "+id);
	}
	
	@GetMapping("all")
	public ResponseEntity<List<Stock>> getAllStock(){
		List<Stock> stocks = service.getAllStocks();
		return ResponseEntity.ok(stocks);
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<Stock> getOneStock(@PathVariable long id){
		try {
			Stock st = service.getOneStock(id);
			return ResponseEntity.ok(st);
		}catch (StockNotFoundException e) {
			throw e;
		}
	}
	
	@PutMapping("update")
	public ResponseEntity<String> updateStock(@RequestBody Stock stock){
		try {
			service.updateStock(stock);
			return ResponseEntity.ok("Stock updated");
		}catch (StockNotFoundException e) {
			throw e;
		}
		
	}

}
