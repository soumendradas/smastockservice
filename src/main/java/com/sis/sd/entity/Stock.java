package com.sis.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.sis.sd.constant.StockType;

import lombok.Data;

@Entity
@Data
@Table(name = "stock_tab")
@DynamicUpdate
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "stock_code_col")
	private String code;
	
	@Column(name = "stock_desc_col")
	private String description;
	
	@Column(name = "stock_cost_col")
	private Double costPerUnit;
	
	@Column(name = "stock_lot_size_col")
	private Integer batchLotSize;
	
	@Column(name = "stock_max_lot_col")
	private Integer maxLotsCust;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "stock_type_col")
	private StockType stockType;
	
	@Column(name = "stock_consumer_id_col")
	private String regConsumerId;
	
	@Column(name = "stock_mode_col")
	private String sourceMode;
	
	//TODO: private Company company;
}
