package com.sis.sd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sis.sd.entity.Stock;
import com.sis.sd.exception.StockNotFoundException;
import com.sis.sd.repository.StockRepository;
import com.sis.sd.service.StockService;

@Service
public class StockServiceImpl implements StockService{

	@Autowired
	private StockRepository repo;
	
	public Long addStock(Stock stock) {
		
		return repo.save(stock).getId();
	}

	
	public Stock getOneStock(Long id) {
		
		return repo.findById(id).orElseThrow(
				()->new StockNotFoundException("Id is not found"));
	}

	
	public List<Stock> getAllStocks() {
		
		return repo.findAll();
	}

	
	public void updateStock(Stock stock) {
		if(repo.existsById(stock.getId()) && stock.getId()!=null) {
			repo.save(stock);
		}else {
			throw new StockNotFoundException("Stock Id is not valid");
		}
	}

}
