package com.sis.sd.service;

import java.util.List;

import com.sis.sd.entity.Stock;

public interface StockService {
	Long addStock(Stock stock);
	Stock getOneStock(Long id);
	List<Stock> getAllStocks();
	void updateStock(Stock stock);
}
