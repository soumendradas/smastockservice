package com.sis.sd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SmaStockServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaStockServiceApplication.class, args);
	}

}
